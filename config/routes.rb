Heliosystem::Engine.routes.draw do
  
  post 'sessions/create', as: 'create_session'
  get 'sessions/destroy', as: 'destroy_session'
  post 'users' => 'users#create', as: 'users'

  root to: 'static#welcome'

  resources :universes, path: '/' do

    resources :people
    resources :subfields, except: [:show, :new]
    resources :subrelations, except: [:show, :new]
    resources :settings, except: [:show, :new]
    resources :tags

    resources :organizations, path: '/o' do
      resources :accounts
      resources :pages
      resources :people
      resources :settings, except: [:show, :new]
      resources :tags
      resources :timeclocks do
        member do
          get 'punches'
          post 'punch'  
        end
      end
      
    end
  end



end
