require 'test_helper'

module Heliosystem
  class StaticControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get welcome" do
      get static_welcome_url
      assert_response :success
    end

  end
end
