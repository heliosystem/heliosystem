require 'test_helper'

module Heliosystem
  class UsersControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    test "should get create" do
      get users_create_url
      assert_response :success
    end

  end
end
