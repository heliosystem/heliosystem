class AddColorToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_column :heliosystem_organizations, :color, :string
  end
end
