class CreateHeliosystemTimepunches < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_timepunches, id: :uuid do |t|
      t.uuid :person_id
      t.uuid :timeclock_id
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps
    end
  end
end
