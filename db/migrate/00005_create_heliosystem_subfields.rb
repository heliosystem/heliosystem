class CreateHeliosystemSubfields < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_subfields, id: :uuid do |t|
      t.string :label
      t.string :format
      t.string :app
      t.uuid :universe_id

      t.timestamps
    end
    add_index :heliosystem_subfields, [:universe_id, :app]
    add_index :heliosystem_subfields, [:universe_id, :label]
    add_index :heliosystem_subfields, [:universe_id, :app, :label]
  end
end
