class CreateHeliosystemAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_accounts, id: :uuid do |t|
      t.string :name
      t.string :code
      t.string :slug
      t.integer :balance_cents, default: 0
      t.uuid :organization_id
      t.uuid :parent_id

      t.timestamps
    end
    add_index :heliosystem_accounts, :slug, unique: true
  end
end
