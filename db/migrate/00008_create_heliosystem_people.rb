class CreateHeliosystemPeople < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_people, id: :uuid do |t|
      t.uuid :universe_id
      t.uuid :user_id
      t.boolean :admin, default: false

      t.timestamps
    end
    add_index :heliosystem_people, :universe_id
    add_index :heliosystem_people, :user_id
  end
end
