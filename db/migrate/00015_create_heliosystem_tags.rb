class CreateHeliosystemTags < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_tags, id: :uuid do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.string :color
      t.uuid :universe_id
      t.uuid :organization_id

      t.timestamps
    end
    add_index :heliosystem_tags, :universe_id
    add_index :heliosystem_tags, [:universe_id, :organization_id]
    add_index :heliosystem_tags, :slug, unique: true
  end
end
