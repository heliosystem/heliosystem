class CreateHeliosystemUniverses < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_universes, id: :uuid do |t|
      t.string :name
      t.text :description
      t.string :slug
      t.string :color
      t.uuid :owner_id

      t.timestamps
    end
    add_index :heliosystem_universes, :name, unique: true
    add_index :heliosystem_universes, :slug, unique: true
  end
end
