class CreateHeliosystemTaggings < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_taggings do |t|
      t.uuid :tag_id
      t.string :taggable_type
      t.uuid :taggable_id

      t.timestamps
    end
    add_index :heliosystem_taggings, :tag_id
    add_index :heliosystem_taggings, [:taggable_type, :taggable_id]
  end
end
