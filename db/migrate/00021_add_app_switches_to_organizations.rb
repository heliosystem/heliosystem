class AddAppSwitchesToOrganizations < ActiveRecord::Migration[6.0]
  def change
    add_column :heliosystem_organizations, :accounts_enabled, :boolean, default: false
    add_column :heliosystem_organizations, :timeclocks_enabled, :boolean, default: false
    add_column :heliosystem_organizations, :pages_enabled, :boolean, default: false
  end
end
