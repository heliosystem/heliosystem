module Heliosystem
  module SettingsHelper
    def setting_icon(setting)
      if setting.id
        if current_organization
          if current_organization.settings.where(key: setting.key).present?
            if current_universe.settings.include?(setting)
              icon('times')
            else
              icon('plus-circle')
            end
          else
            if current_universe.setting(setting.key)
              icon('globe')
            else
              icon('times')
            end
          end
        else
          icon('check')
        end
      else
        icon('plus')
      end
    end
  end
end
