require 'color'
require 'asciidoctor'

module Heliosystem
  module ApplicationHelper
    def action_link path, icon, text, color
	    link_to icon.html_safe, path, style: "background-color: #{color}; color: #{auto_magic_color(color)}", data: {
        toggle: 'popover',
        placement: 'right',
        content: text,
        container: 'body',
        trigger: 'hover'
      }
    end

    def action_button action, icon, text, color
      button_tag icon.html_safe, style: "background-color: #{color}; color: #{auto_magic_color(color)}", data: {
        'nav-action': action,
        toggle: 'popover',
        placement: 'right',
        content: text,
        container: 'body',
        trigger: 'hover'
      }
    end

    def modal_action_button action, icon, text, color
      button_tag icon.html_safe, style: "background-color: #{color}; color: #{auto_magic_color(color)}", data: {
        'modal-target': action,
        toggle: 'popover',
        placement: 'right',
        content: text,
        container: 'body',
        trigger: 'hover'
      }
    end

    def icon(icon, variant="s", *options)
      content_tag(:i, '', class: [
        'fa-fw',
        "fa#{variant}",
        "fa-#{icon}",
        *options
        ])
    end

    def auto_magic_color color
      base = Color::RGB.by_hex(color)
      if base.brightness > 0.5
	'#000'
      else
        '#fff'
      end
    end

    def asciidoc(content)
      Asciidoctor.convert content, safe: :server
    end
  end
end
