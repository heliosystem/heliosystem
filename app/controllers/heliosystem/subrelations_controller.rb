require_dependency "heliosystem/application_controller"

module Heliosystem
  class SubrelationsController < ApplicationController
    before_action :set_subrelation, only: [:show, :edit, :update, :destroy]

    # GET /subrelations
    def index
      @subrelations = current_universe.subrelations.all
    end

    # GET /subrelations/1/edit
    def edit
    end

    # POST /subrelations
    def create
      @subrelation = current_universe.subrelations.build(subrelation_params)

      if @subrelation.save
        redirect_to universe_subrelations_path(@subrelation.universe), notice: 'Subrelation was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /subrelations/1
    def update
      if @subrelation.update(subrelation_params)
        redirect_to universe_subrelations_path(@subrelation.universe), notice: 'Subrelation was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /subrelations/1
    def destroy
      @subrelation.destroy
      redirect_to universe_subrelations_url(current_universe), notice: 'Subrelation was successfully destroyed.'
    end

    private
      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        @breadcrumbs << [universe_subrelations_path(current_universe), 'Custom Relationships Setup']
      end
      # Use callbacks to share common setup or constraints between actions.
      def set_subrelation
        @subrelation = Subrelation.find(params[:id])
        breadcrumbs << [universe_subrelations_path([@subrelation.universe, @subrelation]), @subrelation.label]
      end

      # Only allow a trusted parameter "white list" through.
      def subrelation_params
        params.require(:subrelation).permit(:label, :format, :parent, :child, :universe_id)
      end
  end
end
