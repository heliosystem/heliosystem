require_dependency "heliosystem/application_controller"

module Heliosystem
  class TimeclocksController < ApplicationController
    before_action :set_breadcrumbs
    before_action :set_timeclock, only: [:show, :edit, :update, :destroy, :punch, :punches]

    # GET /timeclocks
    def index
      @timeclocks = current_organization.timeclocks.all
    end

    # GET /timeclocks/1
    def show
    end

    # GET /timeclocks/new
    def new
      @timeclock = current_organization.timeclocks.build
    end

    # GET /timeclocks/1/edit
    def edit
    end

    # POST /timeclocks
    def create
      @timeclock = current_organization.timeclocks.build(timeclock_params)

      sv = timeclock_params.delete(:subfield_values)
      sp = timeclock_params.delete(:subfield_pairs)
      @timeclock.assign_attributes(timeclock_params.except(:subfield_values, :subrelation_pairs))

      if @timeclock.save
        @person.subfield_values = sv
        @person.subrelation_pairs = sp
        @person.save
        redirect_to [current_universe, current_organization, @timeclock], notice: 'Timeclock was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /timeclocks/1
    def update
      if @timeclock.update(timeclock_params)
        redirect_to [current_universe, current_organization, @timeclock], notice: 'Timeclock was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /timeclocks/1
    def destroy
      @timeclock.destroy
      redirect_to universe_organization_timeclocks_url(universe_id: current_universe.slug, organization_id: current_organization.slug), notice: 'Timeclock was successfully destroyed.'
    end

    # GET /timeclocks/1/punches
    def punches
      if params[:person_id]
        @punches = @timeclock.timepunches.where(person_id: params[:person_id])
      else
        @punches = @timeclock.timepunches.all
      end
    end

    # POST /timeclocks/1/punch
    def punch
      @person = current_organization.people.find(params[:timepunch][:person_id])
      puts params[:timepunch].to_json
      if params[:timepunch][:'start_at(1i)'].present?
        @timepunch = @timeclock.timepunches.build(timepunch_params)
        @timepunch.save
        flash.notice = "Made punch @ #{@timepunch.hours}"
      else
        raise 'nope'
        @open_punches = @timeclock.timepunches.where(person_id: params[:timepunch][:person_id]).open
        if @open_punches.present?
          @timepunch = @open_punches.sort_by(&:start_at).last
          @timepunch.end_at = DateTime.now
          @timepunch.save
          flash.notice = "Clocked out @ #{@timepunch.hours}"
        else
          @timepunch = @timeclock.timepunches.create(person_id: params[:timepunch][:person_id], start_at: DateTime.now)
          flash.notice = "Clocked in!"
        end
      end

      redirect_to [current_universe, current_organization, @timeclock]
    end

    private

      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        if current_organization
          @breadcrumbs << [[current_universe, current_organization], current_organization.name]
          @breadcrumbs << [universe_organization_timeclocks_path(current_universe, organization_id: current_organization.slug), 'TimeClocks']
        end
      end

      def set_timeclock
        @timeclock = current_organization.timeclocks.find(params[:id])
        @breadcrumbs << [[current_universe, current_organization, @timeclock], @timeclock.name] if @timeclock.id
      end

      # Only allow a trusted parameter "white list" through.
      def timeclock_params
        params.require(:timeclock).permit(:name, :organization_id, tag_ids: [], subfield_values: {}, subrelation_pairs: {})
      end

      def timepunch_params
        params.require(:timepunch).permit(:person_id, :start_at, :end_at)
      end
  end
end
