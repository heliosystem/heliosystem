require_dependency "heliosystem/application_controller"

module Heliosystem
  class SessionsController < ApplicationController
    skip_before_action :check_for_user, only: [:create]
    def create
      user = User.find_by(email: params[:session][:email].downcase)

      if user && user.authenticate(params[:session][:password])
        session[:user_id] = user.id.to_s
        redirect_to root_path, notice: 'Successfully logged in!'
      else
        flash.now.alert = "Incorrect email or password, try again."
        redirect_to root_path
      end
    end

    def destroy
      session.delete(:user_id)
      redirect_to root_path, notice: "Logged out!"
    end
  end
end
