require_dependency "heliosystem/application_controller"

module Heliosystem
  class AccountsController < ApplicationController
    before_action :set_account, only: [:show, :edit, :update, :destroy]
    before_action :set_breadcrumbs

    # GET /accounts
    def index
	    @accounts = current_organization.accounts.where(parent_id: nil)
    end

    # GET /accounts/1
    def show
    end

    # GET /accounts/new
    def new
	    @account = current_organization.accounts.build
    end

    # GET /accounts/1/edit
    def edit
    end

    # POST /accounts
    def create
      @account = current_organization.accounts.build(account_params)
      @person.assign_attributes(person_params.except(:subfield_values, :subrelation_pairs))

      if @account.parent
        @account.code = @account.parent.code + (current_organization.setting('accounts.separator') || '.') + @account.code
      end

      if @account.save
        @account.subfield_values = sv
        @account.subrelation_pairs = sp
        @account.save
        redirect_to [current_universe, current_organization, @account], notice: 'Account was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /accounts/1
    def update
      @account.update_attributes(account_params)
      if @account.parent
        puts 'doing that thing'
        @account.code = @account.parent.code + (@account.organization.setting('accounts.separator') || '.') + @account.code
      end
      if @account.save
        redirect_to [current_universe, current_organization, @account], notice: 'Account was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /accounts/1
    def destroy
      @account.destroy
      redirect_to universe_organization_accounts_path(universe_id: current_universe.slug, organization_id: current_organization.slug), notice: 'Account was successfully destroyed.'
    end

    private
      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        @breadcrumbs << [[current_universe, current_organization], current_organization.name]
        @breadcrumbs << [universe_organization_accounts_path(current_universe, organization_id: current_organization.slug), 'Accounts']
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_account
	      @account = current_organization.accounts.friendly.find(params[:id])
        @breadcrumbs << [ [current_universe, current_organization, @account], @account.name] if @account.id
      end

      # Only allow a trusted parameter "white list" through.
      def account_params
        params.require(:account).permit(:name, :code, :balance, :parent_id, subfield_values: {}, subrelation_pairs: {}, tag_ids: [])
      end
  end
end
