require_dependency "heliosystem/application_controller"

module Heliosystem
  class StaticController < ApplicationController
    skip_before_action :check_for_user
    def welcome
      @message = "Howdy."
    end
  end
end
