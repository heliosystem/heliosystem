require_dependency "heliosystem/application_controller"

module Heliosystem
  class PeopleController < ApplicationController
    before_action :set_breadcrumbs
    before_action :set_person, only: [:show, :edit, :update, :destroy]

    # GET /people
    def index
      @people = current_container.people.all
    end

    # GET /people/1
    def show
    end

    # GET /people/new
    def new
      @person = current_container.people.build
    end

    # GET /people/1/edit
    def edit
    end

    # POST /people
    def create
      @person = current_container.people.new
      @person.universe = current_universe
      sv = person_params.delete(:subfield_values)
      sp = person_params.delete(:subfield_pairs)
      @person.assign_attributes(person_params.except(:subfield_values, :subrelation_pairs))

      if @person.save
        @person.subfield_values = sv
        @person.subrelation_pairs = sp
        @person.save
        redirect_to [current_container_path, @person].flatten, notice: 'Person was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /people/1
    def update
      if @person.update(person_params)
        redirect_to [current_container_path, @person].flatten, notice: 'Person was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /people/1
    def destroy
      @person.destroy
      redirect_to current_container_path, notice: 'Person was successfully destroyed.'
    end

    private

      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        if current_organization
          @breadcrumbs << [[current_universe, current_organization], current_organization.name]
          @breadcrumbs << [universe_organization_people_path(current_universe, organization_id: current_organization.slug), 'People']
        else
          @breadcrumbs << [universe_people_path(current_universe), 'People']
        end
      end
      
      # Use callbacks to share common setup or constraints between actions.
      def set_person
        @person = current_container.people.find(params[:id])
        if current_organization
          @breadcrumbs << [universe_organization_person_path(@person.universe, @person, organization_id: current_organization.slug), @person.name] if @person.id
        else
          @breadcrumbs << [universe_person_path(@person.universe, @person), @person.name] if @person.id
        end
      end

      def current_container
        current_organization || current_universe
      end

      def current_container_path
        [current_universe, current_organization].filter(&:present?)
      end

      # Only allow a trusted parameter "white list" through.
      def person_params
        params.require(:person).permit(:user_id, :admin, subfield_values: {}, subrelation_pairs: {}, organization_ids: [], tag_ids: [])
      end
  end
end
