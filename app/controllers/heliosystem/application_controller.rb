module Heliosystem
  class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    helper_method :current_user, :current_universe, :current_person, :current_organization, :current_setting, :breadcrumbs
    before_action :breadcrumb_setup
    before_action :check_for_user

    def current_user
      begin
        @current_user ||= User.find(session[:user_id]) if session[:user_id]
      rescue ActiveRecord::RecordNotFound
        session.delete(:user_id)
        nil
      end
    end

    def current_universe
      @current_universe ||= if params[:universe_id]
        Universe.friendly.find(params[:universe_id])
      elsif @universe && @universe.id
        @universe
      end
    end

    def current_person
      current_universe.person_for(current_user)
    end

    def current_organization
      @current_organization ||= if params[:organization_id]
        current_universe.organizations.friendly.find(params[:organization_id])
      elsif @organization && @organization.id
        @organization
      end
    end

    def current_setting(key)
      if current_organization
        current_organization.setting(key)
      else
        current_universe.setting(key)
      end
    end

    def breadcrumb_setup
      @breadcrumbs ||= []
    end

    def breadcrumbs
      @breadcrumbs
    end

    def check_for_user
      redirect_to root_path, notice: 'You have to be logged in to do that' unless current_user
    end

    def check_for_universe_belonging
      if current_universe
        redirect_to root_path, notice: 'You don\'t belong there' unless current_person
      end
    end
  end
end
