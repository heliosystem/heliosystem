require_dependency "heliosystem/application_controller"

module Heliosystem
  class PagesController < ApplicationController
    before_action :set_breadcrumbs
    before_action :set_page, only: [:show, :edit, :update, :destroy]

    # GET /pages
    def index
      @pages = current_organization.pages.all
    end

    # GET /pages/1
    def show
    end

    # GET /pages/new
    def new
      @page = current_organization.pages.build
    end

    # GET /pages/1/edit
    def edit
    end

    # POST /pages
    def create
      @page = current_organization.pages.build(page_params)

      sv = page_params.delete(:subfield_values)
      sp = page_params.delete(:subrelation_pairs)
      @page.assign_attributes(page_params.except(:subfield_values, :subrelation_pairs))

      if @page.save
        @page.subfield_values = sv
        @page.subrelation_pairs = sp
        @page.save
        redirect_to [current_universe, current_organization, @page], notice: 'Page was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /pages/1
    def update
      if @page.update(page_params)
        redirect_to [current_universe, current_organization, @page], notice: 'Page was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /pages/1
    def destroy
      @page.destroy
      redirect_to universe_organization_pages_url(universe_id: current_universe.slug, organization_id: current_organization.slug), notice: 'Page was successfully destroyed.'
    end

    private

      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        if current_organization
          @breadcrumbs << [[current_universe, current_organization], current_organization.name]
          @breadcrumbs << [universe_organization_pages_path(current_universe, organization_id: current_organization.slug), 'Pages']
        end
      end

      def set_page
        @page = current_organization.pages.find(params[:id])
        @breadcrumbs << [[current_universe, current_organization, @page], @page.name] if @page.id
      end

      # Only allow a trusted parameter "white list" through.
      def page_params
        params.require(:page).permit(:title, :slug, :content, tag_ids: [], subfield_values: {}, subrelation_pairs: {})
      end
  end
end
