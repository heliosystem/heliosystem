# CUSTOMIZABLE MODELS
# To make a model customizable:
# 1. Include this concern in to your model: `include Customizable`
# 2. Add `subfield_values: {}, subrelation_pairs: {}` to the list of
#    permitted parameters in your controller
# 3. Because you can't create values/pairs without the parent model existing in
#    the database, you have to do a little workaround in your `create` action.
#    Take a look at `PeopleController#create` for an example.
# 4. Add the `heliosystem/partials/customFormFields` partial to your form. It takes
#    3 locals: `form` (the form object), `model`, the object you're working with,
#    and `app`, the name of the app you're working with.
# 5. Add your app to Heliosystem::Subfield::APPS & Heliosystem::Subrelation::APPS

require 'active_support/concern'

module Heliosystem
  module Customizable
    extend ActiveSupport::Concern

    included do
      has_many :subfield_values, as: :app
      has_many :parent_subrelation_pairs, as: :parent
      has_many :child_subrelation_pairs, as: :child
    end

    def customizable_name
      @customizable_name ||= self.class.to_s.split('::').last.downcase
    end

    def subfield_values=(attrs)
      attrs.each do |k,v|
        if (sf = universe.subfields.find_by(label: k, app: customizable_name)).present?
          sv = subfield_values.where(subfield_id: sf.id).first_or_create
          sv.value = v.to_s
          sv.save
        end
      end if attrs.present?
    end

    def subrelation_pairs
      universe.subrelations.where(parent: customizable_name).map {|sr| sr.subrelation_pairs.where(subrelation_id: sr.id).first_or_create }
    end

    def subrelation_pairs=(attrs)
      attrs.each do |k,v|
        if (sr = universe.subrelations.find_by(label: k, parent: customizable_name)).present?
          pairs = sr.subrelation_pairs
          entities = v.filter(&:present?).map{ |eid| universe.send(sr.child.pluralize.to_sym).find(eid)}
          # remove unnecessary entity pairs
          pairs.each do |p|
            p.delete unless entities.include?(p.child)
          end
          # create missing entity pairs
          entities.each do |et|
            if !pairs.map(&:child).include?(et)
              sr.subrelation_pairs.create(child: et, parent: self)
            end
          end
        end
      end if attrs.present?
    end

  end
end
