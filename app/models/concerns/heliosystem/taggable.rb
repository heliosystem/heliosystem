# TAGGABLE MODELS
# To make a model taggable:
# 1. Include this concern in to your model: `include Customizable`
# 2. Add `tag_ids: []` to the list of permitted parameters in your controller
# 3. Add the `heliosystem/partials/taggableFields` partial to your form. It
#    takes 2 locals: `form` (the form object), and `model`, the object
#    you're working with.

require 'active_support/concern'

module Heliosystem
  module Taggable
    extend ActiveSupport::Concern

    included do
      has_many :taggings, as: :taggable
      has_many :tags, through: :taggings
    end
  end
end
