require 'money'

module Heliosystem
  class Account < ApplicationRecord
    extend FriendlyId
    friendly_id :code, use: :slugged
    include Customizable
    include Taggable
    belongs_to :organization
    belongs_to :parent, class_name: 'Account', optional: true, inverse_of: :children
    has_many :children, class_name: 'Account', dependent: :destroy, inverse_of: :parent, foreign_key: 'parent_id'
    before_save :update_parent_balances

    def balance
      Money.new(balance_cents, 'usd')
    end

    def balance=(val)
      self.balance_cents = BigDecimal(val) * 100
    end

    def depth
      (parent ? parent.depth + 1 : 0)
    end

    def universe
      organization.universe
    end

    def subcode
      code.split(organization.setting('accounts.separator') || '.').last
    end

    private

    def update_parent_balances
      return unless parent && balance_cents_changed?
      puts "Updating balance for #{parent.name}"
      delta = changes['balance_cents'][1] - changes['balance_cents'][0]
      parent.balance_cents += delta
      parent.save
    end
  end
end
