module Heliosystem
  class Page < ApplicationRecord
    include Customizable
    include Taggable
    belongs_to :organization

    def name
      title
    end

    def universe
      organization.universe
    end
  end
end
