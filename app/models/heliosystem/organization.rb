module Heliosystem
  class Organization < ApplicationRecord
    extend FriendlyId
    friendly_id :name, use: :slugged
    belongs_to :universe

    has_many :accounts
    has_many :memberships, dependent: :destroy
    has_many :pages
    has_many :people, through: :memberships
    has_many :settings
    has_many :tags
    has_many :timeclocks

    def setting(key)
      if s = settings.where(key: key).first
        s.value
      else
        universe.setting(key)
      end
    end

  end
end
