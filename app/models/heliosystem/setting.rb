module Heliosystem
  class Setting < ApplicationRecord

    def organization
      Organization.find(organization_id) if organization_id
    end

    def universe
      organization.universe if organization
      Universe.find(universe_id)
    end

    def container
      organization || universe
    end
  end
end
