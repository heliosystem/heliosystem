module Heliosystem
  class Subfield < ApplicationRecord
    belongs_to :universe
    has_many :subfield_values, dependent: :destroy

    APPS = %w{person account timeclock page}
    FORMATS = %w(boolean string email url tel password text integer decimal datetime date time select)
  end
end
