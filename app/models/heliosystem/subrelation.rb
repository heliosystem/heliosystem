module Heliosystem
  class Subrelation < ApplicationRecord
    belongs_to :universe
    has_many :subrelation_pairs, dependent: :destroy

    APPS = %w{person event timeclock page}
    FORMATS = %w{one-to-one one-to-many many-to-many}
  end
end
