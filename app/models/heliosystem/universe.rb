module Heliosystem
  class Universe < ApplicationRecord
    extend FriendlyId
    friendly_id :name, use: :slugged
    belongs_to :owner, class_name: "User"
    has_many :subfields
    has_many :subrelations
    has_many :people
    has_many :organizations
    has_many :settings
    has_many :tags
    has_many :timeclocks, through: :organizations

    def users
      User.where(id: owner_id)
    end

    def top_level_tags
      tags.where(organization_id: nil)
    end

    def admins
      people.where(admin: true)
    end

    def owner_candidates
      (User.where(id: owner_id).all + admins.map(&:user)).uniq
    end

    def person_for(user)
      if user
        people.find_by(user_id: user.id)
      else
        nil
      end
    end

    def setting(key)
      if s = settings.where(key: key).first
        s.value
      else
        nil
      end
    end
  end
end
