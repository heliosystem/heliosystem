module Heliosystem
  class Timepunch < ApplicationRecord
    belongs_to :timeclock
    belongs_to :person

    scope :open, -> { where(end_at: nil) }
    scope :closed, -> { where.not(end_at: nil) }

    def hours
      return nil unless end_at.present?
      ((end_at - start_at) / 60 / 60 * 4).round / 4.0
    end
  end
end
