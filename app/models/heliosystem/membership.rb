module Heliosystem
  class Membership < ApplicationRecord
    belongs_to :organization
    belongs_to :person
  end
end
