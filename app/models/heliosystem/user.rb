module Heliosystem
  class User < ApplicationRecord
    has_secure_password
    validates :email, presence: true, uniqueness: true
    has_many :people
    has_many :universes, through: :people

    def owned_universes
      Universe.where(owner_id: id)
    end

    def name
      first_name + ' ' + last_name
    end
  end
end
